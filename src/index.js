const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const mongoose = require('mongoose');
const config = require('./config');
const movieRoutes = require('./app/routes/moviesRoutes');
const swaggerRoutes = require('./app/routes/swaggerRoutes');
const swagger = require('swagger2');
const { ui } = require('swagger2-koa');

const swaggerDocument = swagger.loadDocumentSync("./src/api.yaml");

const app = new Koa();

app.use(bodyParser());

app.use(ui(swaggerDocument, "/swagger"))
    .use(swaggerRoutes.routes())
    .use(swaggerRoutes.allowedMethods());

app.use(movieRoutes.routes()).use(movieRoutes.allowedMethods());

mongoose.connect(config.db, (error, res) => {
    if (error) {
        return console.log(`Error: Unable to establish database connection: ${error}`);
    }
    console.log('Connection to the established database...');
    app.listen(config.port);
    console.log(`Appication is running on port ${config.port}`);
});