const Movie = require('../models/movie');
const ORDER = 'asc';

function saveMovie(req) {
    let movie = new Movie();
    movie.title = req.title;
    movie.year = req.year;
    movie.released = req.released;
    movie.genre = req.genre;
    movie.director = req.director;
    movie.actors = req.actors;
    movie.plot = req.plot;
    movie.ratings = req.ratings;
    return movie.save();
}

function getMovie(title, year) {
    let regex = new RegExp(title, 'i');
    const LIMIT = 1;
    if (year === undefined) {
        return Movie.find({ title: { $regex: regex } }).sort({ title: ORDER }).limit(LIMIT);
    }

    return Movie.find({ title: { $regex: regex } }).find({ year: year }).sort({ title: ORDER }).limit(LIMIT);
}

function getMovies(page) {
    const PER_PAGE = 5;
    page = Math.max(0, page);
    let nextPage = PER_PAGE * page;
    return Movie.find().sort({ title: ORDER }).limit(PER_PAGE).skip(nextPage);
}

function replacePlot(movie, movieFilter) {
    return movie.plot.replaceAll(movieFilter.find, movieFilter.replace);
}

module.exports = {
    saveMovie,
    getMovie,
    getMovies,
    replacePlot
}