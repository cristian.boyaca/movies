const Joi = require('@hapi/joi');

const schema = Joi.object({
    page: Joi.number().required()
});

module.exports = schema;