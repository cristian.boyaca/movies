const Joi = require('@hapi/joi');

const schema = Joi.object({
    title: Joi.string().required(),
    year: Joi.string().required(),
    released: Joi.string().required(),
    genre: Joi.string().required(),
    director: Joi.string().required(),
    actors: Joi.string().required(),
    plot: Joi.string().required(),
    ratings: Joi.array().items({ source: Joi.string().required(), value: Joi.string().required() })
});

module.exports = schema;