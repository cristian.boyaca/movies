function getErrorMessage(status, error) {
    error = error.replaceAll("\"", "");
    let jsonError = `{"status":${status}, "error":"${error}"}`;
    return JSON.parse(jsonError);
}

module.exports = {
    getErrorMessage
}