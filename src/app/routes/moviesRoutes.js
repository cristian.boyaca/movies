const Router = require("@koa/router");
const movieController = require('../controllers/movieController');
const exceptionResponse = require('../exception/exceptionResponse');
const movieSchema = require('../schemas/movie');
const movieSearchSchema = require('../schemas/movieSearch');
const moviesSearchSchema = require('../schemas/moviesSearch');
const movieReplaceSchema = require('../schemas/movieReplace');
const validate = require('../middlewares/validateData');

const router = new Router({
    prefix: '/movies'
});

router.post('/', validate.validateBody(movieSchema), async ctx => {
    let movie = ctx.request.body;
    try {
        let movieStored = await movieController.saveMovie(movie);
        ctx.response.status = 201;
        ctx.body = movieStored;
    } catch (error) {
        ctx.response.status = 500;
        ctx.body = exceptionResponse.getErrorMessage(ctx.response.status, `Failed to save in database: ${error} `);
    }
});

router.get('/search', validate.validateQueryParams(movieSearchSchema), async ctx => {
    let title = ctx.query.title;
    let year = ctx.request.header["year"];
    ctx.body = await movieController.getMovie(title, year);
    ctx.response.status = 200;
});

router.get('/', validate.validateHeaders(moviesSearchSchema), async ctx => {
    let page = ctx.request.header["page"]
    ctx.body = await movieController.getMovies(page);
    ctx.response.status = 200;
});

router.post("/replace", validate.validateBody(movieReplaceSchema), async ctx => {
    let movieFilter = ctx.request.body;
    let movie = await movieController.getMovie(movieFilter.movie, undefined);
    if (movie.length > 0) {
        movie = movie[0]._doc;
        ctx.body = movieController.replacePlot(movie, movieFilter);
        ctx.response.status = 200;
    } else {
        ctx.response.status = 404;
        ctx.body = exceptionResponse.getErrorMessage(ctx.response.status, `Movie not found: ${movieFilter.movie} `);
    }
});

module.exports = router;
