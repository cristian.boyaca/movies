const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MovieSchema = Schema({
    title: { type: String, unique: true },
    year: String,
    released: String,
    genre: String,
    director: String,
    actors: String,
    plot: String,
    ratings: [{ source: String, value: String }]
});

module.exports = mongoose.model('Movie', MovieSchema);