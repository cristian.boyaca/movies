const Boom = require('@hapi/boom');
const exceptionResponse = require('../exception/exceptionResponse');

function validateBody(schema) {
    return async (ctx, next) => {
        try {
            await schema.validateAsync(ctx.request.body);
            await next();
        } catch (error) {
            ctx.response.status = 400;
            ctx.body = exceptionResponse.getErrorMessage(ctx.response.status, Boom.badData(error).message);
        }
    };
}

function validateQueryParams(schema) {
    return async (ctx, next) => {
        try {
            await schema.validateAsync(ctx.query);
            await next();
        } catch (error) {
            ctx.response.status = 400;
            ctx.body = exceptionResponse.getErrorMessage(ctx.response.status, Boom.badData(error).message);
        }
    };
}

function validateHeaders(schema) {
    return async (ctx, next) => {
        try {
            ctx.request.header
            await schema.validateAsync(ctx.request.header);
            await next();
        } catch (error) {
            ctx.response.status = 400;
            ctx.body = exceptionResponse.getErrorMessage(ctx.response.status, Boom.badData(error).message);
        }
    };
}

module.exports = { validateBody, validateQueryParams, validateHeaders }