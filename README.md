# movies

Este es un ejemplo de servidor de películas

### Pre-requisitos 📋

* Instalar nodeJS, npm,MongoDB o instalar docker y docker compose

## Despliegue 📦

Se puede correr la aplicación usando docker o node.De la siguiente forma:

* Docker Compose

```
docker-compose build  //Construir imagenes contenidas en el archivo del docker compose
docker-compose up  //Correr los contenedores de las imagenes
docker-compose down // Detener los contenedores
```

* Node

```
npm i
npm start
```

## Documentación de los servicios

* Correr la aplicación e ingresar
```
http://localhost:3000/swagger
```